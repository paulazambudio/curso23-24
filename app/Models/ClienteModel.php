<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\Models;
use CodeIgniter\Model;

/**
 * Description of ClienteModel
 *
 * @author a023319134d
 */
class ClienteModel extends Model{
    protected $table    = 'clientes';
    protected $primaryKey   = 'id';
    protected $useAutoIncrement = true; 
    protected $returnType   = 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
    protected $allowedFields    = ['nombre', 'apellido1', 'apellido2', 'email']; // Se supone que sin esto también funciona

    
    // VALIDACIÓN
    //Reglas de validación para cada campo
    protected $validationRules = [
        'id' => 'numeric', //Podría dar error, porque detecta que email está allí, así qu ehay que especificar el id
        'nombre' => 'required|max_length[30]|min_length[3]',
        'apellido1' => 'required|max_length[30]|alpha_numeric_space|min_length[3]',
        'apellido2'     => 'trim|strtoupper',
        'email' => 'required|max_length[254]|valid_email|is_unique[clientes.email,id,{id}]', //Aquí hay que poner esto del id tmb
    ];
    //Mensajes de validación
    protected $validationMessages = [
        'email' => [
            'is_unique' => 'El correo electrónico ya está usado por otro usuario.',
            'required' => 'No puedes dejar el email en blanco.',
            'valid_email' => 'El email no es válido',
            'max_length' => 'El email excede los caracteres máximos'
        ],
        'nombre' => [
            'required' => 'No puedes dejar el nombre en blanco',
            'max_length' => 'El nombre excede los caracteres máximos',
            'min_length' => 'El nombre no cumple los caracteres'
        ],
        'apellido1' => [
            'required' => 'No puedes dejar el apellido 1',
            'max_length' => 'El apellido 1 excede los caracteres máximos',
            'alpha_numeric_space' => 'No puedes incluir símbolos en el apellido 1'
        ],
        'id' => [
            'numeric' => 'El id solo puede ser numérico carechimba'
        ],
    ];
    
}
