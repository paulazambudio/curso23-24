<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of MedicoModel
 *
 * @author a023319134d
 */
class MedicoModel extends Model{
    protected $table    = 'medicos';
    protected $primaryKey   = 'id';
    protected $useAutoIncrement = true; 
    protected $returnType   = 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
    protected $allowedFields    = ['nombre', 'apellido1', 'apellido2', 'email', 'especialidades']; // Se supone que sin esto también funciona

    // VALIDACIÓN
    //Reglas de validación para cada campo
    protected $validationRules = [
        'id' => 'numeric', //no importa, es para que revise la id a colación de la edición, porque lo usamos en email
        'nombre' => 'required|max_length[30]|min_length[3]',
        'apellido1' => 'required|max_length[30]|alpha_numeric_space|min_length[3]',
        'apellido2'     => 'trim|strtoupper',
        'email' => 'required|max_length[254]|valid_email|is_unique[medicos.email,id,{id}]',
        'especialidades' => 'required|is_not_unique[especialidades.id]',
    ];
    //Mensajes de validación
    protected $validationMessages = [
        'email' => [
            'is_unique' => 'El correo electrónico ya está usado por otro usuario.',
            'required' => 'No puedes dejar el email en blanco.'
        ],
        'nombre' => [
            'required' => 'No puedes dejar el nombre en blanco.',
            'max_length' => 'No puede superar los 30 caracteres.',
            'min_length' => 'No puede ser inferior a 3 caracteres.'
        ],
    ];
}
