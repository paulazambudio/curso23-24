<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of EspecialidadModel
 *
 * @author a023319134d
 */
class EspecialidadModel extends Model{
    protected $table    = 'especialidades';
    protected $primaryKey   = 'id';
    //protected $useAutoIncrement = true; 
    protected $returnType   = 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
    protected $allowedFields    = ['nombre']; // Se supone que sin esto también funciona

    // VALIDACIÓN
    //Reglas de validación para cada campo
    protected $validationRules = [
        'id' => 'numeric',
        'nombre' => 'required|max_length[30]|min_length[3]|is_unique[especialidades.nombre,id,{id}]',
    ];
    //Mensajes de validación
    protected $validationMessages = [
        'nombre' => [
            'is_unique' => 'La especialidad ya existe.',
            'required' => 'No puedes dejar el nombre en blanco.'
        ],
    ];
    
}
