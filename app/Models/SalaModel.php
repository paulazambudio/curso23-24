<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of SalaModel
 *
 * @author a023319134d
 */
class SalaModel extends Model{
    protected $table    = 'salas';
    protected $primaryKey   = 'id';
    protected $useAutoIncrement = true; 
    protected $returnType   = 'object';  // array/object -> No cambia exactamente(estructura de variable distinta), él prefiere object
    protected $allowedFields    = ['nombre']; // Se supone que sin esto también funciona

    
    // VALIDACIÓN
    //Reglas de validación para cada campo
    protected $validationRules = [
        'id' => 'numeric',
        'nombre' => 'required|max_length[30]|min_length[3]|is_unique[salas.nombre,id,{id}]',
    ];
    //Mensajes de validación
    protected $validationMessages = [
        'nombre' => [
            'is_unique' => 'El nombre de esta sala ya está en uso.',
            'required' => 'No puedes dejar el nombre en blanco.',
            'max_length' => 'No puede superar los 30 caracteres.',
            'min_length' => 'No puede ser inferior a 3 caracteres.'
        ],
    ];
    
}
