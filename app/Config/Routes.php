<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/indice', 'Php::enlaces');
$routes->get('/tablas', 'Php::tablas');
$routes->get('/tablas/(:num)', 'Php::tablas/$1');
$routes->get('/facto/(:num)', 'Php::facto/$1');
$routes->get('/contar/(:alpha)', 'Php::contarletras/$1');
$routes->get('/sumar', 'Php::sumar');
$routes->get('/vocales/(:alpha)', 'Php::contarvocales/$1');
$routes->get('/veces/(:num)', 'Php::cuantova/$1');
$routes->get('/vecesbis/(:num)', 'Php::cuantov/$1');
$routes->post('/letranif', 'Php::letranif');
$routes->get('/formulti', 'Php::formulti');
$routes->post('/multi', 'Php::multi');
$routes->get('/formoperar', 'Php::formoperar');
$routes->post('/operar', 'Php::operar');
$routes->get('/formulario', 'Php::formulario');
$routes->get('/tabla7', 'Php::tabla7');
$routes->get('/tabla7/(:num)', 'Php::tabla7/$1');
$routes->get('/calendar', 'calendar::calendar');
$routes->get('/calendar/(:alpha)', 'calendar::calendar/$1');
$routes->get('/calendar/(:num)', 'calendar::calendar/$1');
$routes->get('/muestra', 'calendar::mues_mes');
$routes->get('/muestra/(:num)/(:num)', 'calendar::mues_mes/$1/$2');
$routes->get('/mostrarmes', 'calendar::muestra_mes');
$routes->get('/mostrarmes/(:num)/(:num)', 'calendar::muestra_mes/$1/$2');

//Faker
$routes->get('/faker', 'MyFaker::index');
$routes->get('/imagenes', 'MyFaker::imagenes');
$routes->get('/faker/clientes', 'MyFaker::insertarClientes');
$routes->get('/faker/clientes/(:num)', 'MyFaker::insertarClientes/$1');
$routes->get('/faker/medicos', 'MyFaker::insertarMedico');
$routes->get('/faker/medicos/(:num)', 'MyFaker::insertarMedico/$1');


//clientes
$routes->get('/clientes', 'ClienteController::index');
$routes->get('clientes/form','ClienteController::formcli');
$routes->post('clientes/form','ClienteController::formcli');
$routes->get('/clientes/editar/(:num)','ClienteController::formCliente_edit/$1');//TENGO DUDAS
$routes->post('/clientes/editar/(:num)','ClienteController::formCliente_edit/$1');
//$routes->post('/clientes/guardar_edit/(:num)','ClienteController::guardarcli_edit/$1');
$routes->get('/clientes/borrar/(:num)','ClienteController::borrarcli/$1');

//medicos
$routes->get('/medicos', 'MedicoController::index');
$routes->get('/medicos/form','MedicoController::form');
$routes->post('/medicos/form','MedicoController::form');
$routes->get('/medicos/editar/(:num)','MedicoController::form_edit/$1');//TENGO DUDAS
$routes->post('/medicos/editar/(:num)','MedicoController::form_edit/$1');
//$routes->post('/medicos/guardar_edit/(:num)','MedicoController::guardar_edit/$1');
$routes->get('/medicos/borrar/(:num)','MedicoController::borrar/$1');



//salas
$routes->get('/salas', 'SalaController::index');
$routes->get('/salas/form','SalaController::formsala');
$routes->post('/salas/form','SalaController::formsala');
$routes->get('/salas/editar/(:num)','SalaController::formsala_edit/$1');
$routes->post('/salas/editar/(:num)','SalaController::formsala_edit/$1');
//$routes->post('/salas/guardar_edit/(:num)','SalaController::guardarsala_edit/$1');
$routes->get('/salas/borrar/(:num)','SalaController::borrarsala/$1');

//especialidades
$routes->get('/especialidades', 'EspecialidadController::index');
$routes->get('/especialidades/form','EspecialidadController::formespecial');
$routes->post('/especialidades/form','EspecialidadController::formespecial');
$routes->get('/especialidades/editar/(:num)','EspecialidadController::formespecial_edit/$1');
$routes->post('/especialidades/editar/(:num)','EspecialidadController::formespecial_edit/$1');
//$routes->post('/especialidades/guardar_edit/(:num)','EspecialidadController::guardarespecial_edit/$1');
$routes->get('/especialidades/borrar/(:num)','EspecialidadController::borrarespecial/$1');


//SubirArchivo
$routes->get('/upload','SubirArchivo::index');
$routes->post('/upload_subir','SubirArchivo::upload');