<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of Php
 *
 * @author a023319134d
 */

class Php extends BaseController{
    //put your code here
    public function enlaces() {
        for ($i=0; $i<=8;$i++){//repite 9 veces
            echo "<p>";
            echo "<a href='http://ed$i.ausias.net' title='Equipo de trabajo $i'> ";
            echo "Equipo de trabajo nº$i";
            echo "</p>\n";
        }
    }
    public function tablasiete() {
        for ($i=0; $i<=10;$i++){
            echo "<p>";
            echo "7 x $i = " ,7*$i;
            echo "</p>\n";   
        }
        
    }
    
    public function tablas(int $num = 7) {
        for ($i=0; $i<=10;$i++){
            echo "<p>";
            echo "$num x $i = " ,$num*$i;
            echo "</p>\n";   
        }
        
    }
     public function facto(int $num = 7) {
         $total=0;
         for ($i=$num; $i=1;$i--){
            echo "<p>";
            echo "El factorial de $num es ";
            echo "</p>\n";   
        }
        
    }
    
    public function contarletras($palabra) {
       $contador = 0;
       for ($i=0;$i<strlen($palabra);$i++){
           if(in_array(strtoupper($palabra[$i])  )){
               $contador++;
           }
       }
       echo "La palabra $palabra tiene $contador aes";
        
    }
    /**********************************
     * Suma los números de un array
     **********************************/
    public function sumar(){
        $vector = [6,7,8,9,2,12,21,2,6,6];
        $sum = 0;
        foreach($vector as $elemento){
            $sum+=$elemento;
        }
        echo "La suma de todos los elementos del vector es $sum";
    }
    public function contarvocales($palabra) {
	$vocales = ['a', 'e', 'i', 'o', 'u'];
	$conteo = 0;

	for ($i=0;$i<strlen($palabra);$i++) {
    		$letra = strtolower($palabra[$i]);
    		if (in_array($letra, $vocales)) {
        		$conteo++;
    		}
	}

	echo "La cantidad de vocales en '$palabra' es: $conteo";
    }
    /*
     * Solución Profesor
    public function contarvocales($palabra){    
        $contador=0;
        for ($i=0;$i<strlen($palabra);$i++) {
            if (strpos("AaEeIiOoUu",$palabra[$i])!==FALSE){
                $contador++;
            }
        }
        echo "La palabra $palabra" tiene $contador vocales"
    }    
     */
    /*************************************
     * Contar el número de veces que aparece un elemento que se pasa
     * como parámetro en un array que se define en la función
     *************************************/
    public function cuantova($num) {
        $vector = [6,7,8,9,2,12,21,2,6,6];
        $vector[23]=22; $vector[13]=45;
        $sum = 0;
        foreach($vector as $elemento){
            if ($elemento==$num){
                $sum++;
            }
        }
        echo "El nº $num aparece: $sum veces";
    }
    /*****************************************
     * Actividad anterior modificada
     * saca el indice de lo que aparece en el vector
     * y mas
     *****************************************/
    public function cuantov($num) {
        $vector = [6,7,8,9,2,12,[21,2,6],6,5,94];
        $vector[23]=22; $vector['naranjas']=22; $vector[13]=6;
        $sum = 0;
        foreach($vector as $indice => $elemento){
            echo "$indice<br>\n";
            if ($elemento==$num){
                $sum++;
            }
        }
        echo "El nº $num aparece: $sum veces";
        echo '<pre>';
        print_r($vector); //mostrar estructuras complejas (no solo str)
    }
        
    /***********************************************************
    * Cálculo del dígito de control del NIF
    **********************************************************/
    public function letranif(){
        $dni = $this->request->getPost('numero');
        echo "La letra del $dni es" .$this->calcularletra($dni);              
    }
    
    /***********************************************************
     * Cálculo del dígito de control del NIE
     **********************************************************/

    /********************************************************
     * NUESTRO PRIMER Y MÁS BÁSICO FORMULARIO
     *******************************************************/
    public function formulario(){
        echo "<form action='letranif' method='post'>";
            echo "<input type='text' name='numero'>";
            echo "<input type='submit'>";
        echo "</form>";
    }
    private function calcularletra($documento){
        $letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        $resto = $documento % 23;
        return $letras[$resto];
    }
    /******************************************************
     * formulario 2 campos íntegros multiplicar
     ****************************************************/
    public function formulti(){
        echo "<form action='multi' method='post'>"; //multi envia a la otra funcion 
            echo "<input type='number' name='numero1'>"; //este campo es para el 1º
            echo "<input type='number' name='numero2'>";// para el 2º num
            echo "<input type='submit'>"; //enviar datos
        echo "</form>";
    }
    
    public function multi(){
    /*la movida de $this->request->getPost('xxxxx') sirve para llamar al formulario 
     * "formulti" para asignar  a la variable $uno el numero que se haya metido 
     * en numero1 y así ya la tenemos aquí ***********************************/
        $uno = $this->request->getPost('numero1');                                    
        $dos = $this->request->getPost('numero2');
        $resultado = $uno * $dos;
        echo "El resultado de la multiplicación es $resultado";
        #echo "La multiplicación de $uno por $dos es" .$this->multi($resultado);
        //esta era la versión complicada que no vamos a llevar a cabo
    }
    
    /*********************************************
     * FORMULARIO MULTIOPERACION RADIOBUTTON
     *********************************************/
    
    public function formoperar(){
        echo "<form action='operar' method='post'>"; 
            echo "<input type='number' name='numero1'><br>"; //este campo es para el 1º
            echo "<input type='number' name='numero2'><br>";// para el 2º num
            /************ ahora vamos a poner el radio button un poco a lo salvaje
             * lo importante es que todo se llame "operador" y cambiar el
             * valor para cada tipo de operación (valor no opera, es otro nombrecito) 
             **********/
            echo "<input type='radio' id='suma' name='operador' value='suma'>
            <label for='suma'>SUMA</label>";
            echo "<input type='radio' id='resta' name='operador' value='resta'>
            <label for='resta'>RESTA</label>";
            echo "<input type= 'radio' id='multiplica' name='operador' value='multiplica'>
            <label for='multiplica'>MULTIPLICA</label>";
            echo "<input type= 'radio' id='divide' name='operador' value='divide'>
            <label for='divide'>DIVIDE</label><br>";
            echo "<input type='submit'>"; //enviar datos
        echo "</form>";
    }
    
    public function operar(){
    /*la movida de $this->request->getPost('xxxxx') sirve para llamar al formulario 
     * "formoperar" para asignar  a la variable $uno el numero que se haya metido 
     * en numero1 y así ya la tenemos aquí ***********************************/
        $uno = $this->request->getPost('numero1');                                    
        $dos = $this->request->getPost('numero2');
        switch ($this->request->getPost('operador')) {
            case 'suma':
                $resultado = $uno + $dos;
                echo "La suma de $uno y $dos sería = $resultado";
                break;
            case 'resta':
                $resultado = $uno - $dos;
                echo "La resta de $uno y $dos sería = $resultado";
                break;
            case 'multiplica':
                $resultado = $uno * $dos;
                echo "La multiplicación entre $uno y $dos sería = $resultado";
                break;
            case 'divide':
                $resultado = $uno / $dos;
                echo "La división entre $uno y $dos sería = $resultado";
                break;
        } 
    }
    
    public function calculador(){
        $valores=$this->request->getPost();
        echo "<pre>";
        print_r($valores);  
    }
    
    /**************************************
     * tabla del 7 para intro a calendario
     ***************************************/
    public function tabla7(int $num = 7) {
        $data['numero'] = $num;
        return view('tabla/tabla', $data);
    }
}
