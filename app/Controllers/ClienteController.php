<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ClienteModel;

/**
 * Description of ClienteController
 *
 * @author a023319134d
 */
class ClienteController extends BaseController {
// controlador que mostrará todos los clientes
    public function index(){
        $clientes = new ClienteModel();
	$data['titulo'] = 'Clientes'; 
        $data['clientes'] = $clientes->findAll(); // print_r($clientes->find(4)); -> busca/encuentra el id=4
        return view('/dentista/cliente/Tabla',$data);
    }
    
    
    public function formcli(){
        Helper('form');
        $data['titulo'] = 'Alta Cliente';
        
        if (strtoupper($this->request->getMethod())=='GET'){ //mostramos el formulario
            
            return view('dentista/cliente/FormCliente',$data); 
        } else {
            $cliente = $this->request->getPost();
            unset($cliente['enviar']);
            $clienteModel = new ClienteModel(); //crear el objeto
            if ($clienteModel->insert($cliente)=== false){ //He encontrado un error
                //quiero mostrar los errores
                $data['errores'] = $clienteModel->errors();
                return view('dentista/cliente/FormCliente',$data);
            } else {
                return redirect('clientes');
            }
        }
        
        //return view('/dentista/cliente/FormCliente',$data);
    }
    
    //Ya no la utilizamos, porque cuando añadimos utilizamos la anterior función
    public function guardarcliente(){
        $cliente = $this->request->getPost();
        unset($cliente['enviar']);
        /*echo '<pre>';
        print_r($cliente);
        echo '</pre>';*/
        $clienteModel = new ClienteModel(); //crear el objeto
        $clienteModel->insert($cliente);
        return redirect('clientes');

    }
    
    
    public function formCliente_edit($id){
        Helper('form');
        $data['titulo'] = 'Modificación Cliente';
        
               
    //de aquí pabajo es distinto al form de entrada
        $clienteModel = new ClienteModel(); //para acceder a la BD
        $data['cliente'] = $clienteModel->find($id);//esta id la tengo que poner en el parámetro de form_edit para decirle a quién edito
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('dentista/cliente/FormCliente_edit',$data);
        } else {
            $cliente = $this->request->getPost();
            unset($cliente['enviar']);
            $cliente['id'] = $id;//Se pone el id, lo que se dice en el ClienteModel
            
            $clienteModel = new ClienteModel(); //crear el objeto
            if ($clienteModel->update($id,$cliente)===false){
               //quiero mostrar los errores
                $data['errores'] = $clienteModel->errors();
                return view('dentista/cliente/FormCliente_edit',$data); 
            } else {
                return redirect('clientes');
            }
        }
        
      
        //nos muestra el formulario para editar
        //return view('dentista/cliente/FormCliente_edit',$data);
        
    }
    
    
    public function guardarcli_edit($id){
        
        $cliente = $this->request->getPost();
        unset($cliente['enviar']);
        //echo '<pre>';
        //print_r($cliente);
        //echo '</pre>';
        $clienteModel = new ClienteModel(); //crear el objeto
        $clienteModel->update($id,$cliente);
        return redirect('clientes');

    }
    

    
    public function borrarcli($id){
        $clienteModel = new ClienteModel(); //crear el objeto
        //borrar
        $clienteModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('clientes');

    }   
    
    
    
}
