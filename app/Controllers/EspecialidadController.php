<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\EspecialidadModel;

/**
 * Description of EspecialidadController
 *
 * @author a023319134d
 */
class EspecialidadController extends BaseController {
// controlador que mostrará todos los medicos
    public function index(){
        $especialidades = new EspecialidadModel();
	$data['titulo'] = 'Especialidades'; 
        $data['especialidades'] = $especialidades->findAll();
        return view('/dentista/especialidad/Tablaespecial',$data);
    }
    
    public function formespecial(){
        Helper('form');
        $data['titulo'] = 'Añadir Especialidad';
        
        if (strtoupper($this->request->getMethod())=='GET'){ //mostramos el formulario
            
            return view('dentista/especialidad/Formespecial',$data); 
        } else {
            $especialidad = $this->request->getPost();
            unset($especialidad['enviar']);
            $especialidadModel = new EspecialidadModel(); //crear el objeto
            if ($especialidadModel->insert($especialidad)=== false){ //He encontrado un error
                //quiero mostrar los errores
                $data['errores'] = $especialidadModel->errors();
                return view('dentista/especialidad/Formespecial',$data);
            } else {
                return redirect('especialidades');
            }
        }
        
        //return view('/dentista/especialidad/Formespecial',$data);
    }
    
    //No se utiliza, ya insertamos con la funcion de arriba
    public function guardarespecial(){
        $especialidad = $this->request->getPost();
        unset($especialidad['enviar']);
        /*echo '<pre>';
        print_r($cliente);
        echo '</pre>';*/
        $especialidadModel = new EspecialidadModel(); //crear el objeto
        $especialidadModel->insert($especialidad);
        return redirect('especialidades');

    }
    
    
    public function formespecial_edit($id){
        Helper('form');
        $data['titulo'] = 'Modificación Especialidad';
        
               
    //de aquí pabajo es distinto al form de entrada
        $especialidadModel = new EspecialidadModel(); //para acceder a la BD
        $data['especialidad'] = $especialidadModel->find($id);//esta id la tengo que poner en el parámetro de form_edit para decirle a quién edito
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('dentista/especialidad/Formespecial_edit',$data);
        } else {
            $especialidad = $this->request->getPost();
            unset($especialidad['enviar']);
            $especialidad['id'] = $id;
            
            $especialidadModel = new EspecialidadModel(); //crear el objeto
            if ($especialidadModel->update($id,$especialidad)===false){
               //quiero mostrar los errores
                $data['errores'] = $especialidadModel->errors();
                return view('dentista/especialidad/Formespecial_edit',$data); 
            } else {
                return redirect('especialidades');
            }
        }
        
      
        //nos muestra el formulario para editar
        //return view('dentista/especialidad/Formespecial_edit',$data);
        
    }
    
    
    public function guardarespecial_edit($id){
        
        $especialidad = $this->request->getPost();
        unset($especialidad['enviar']);
        //echo '<pre>';
        //print_r($especialidad);
        //echo '</pre>';
        $especialidadModel = new EspecialidadModel(); //crear el objeto
        $especialidadModel->update($id,$especialidad);
        return redirect()->to('especialidades');

    }
    

    
    public function borrarespecial($id){
        $especialidadModel = new EspecialidadModel(); //crear el objeto
        //borrar
        $especialidadModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('especialidades');

    }   
    
}