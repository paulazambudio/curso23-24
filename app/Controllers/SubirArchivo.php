<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of SubirArchivo
 *
 * @author a023319134d
 */

class SubirArchivo extends BaseController {
    //put your code here
    
    public function index()
    {
        Helper('form');
        $data['titulo'] = 'Sube tu archivo';
        return view('subirarchivo/archivo', $data);
    }
    public function upload() {
        Helper('form');
                
        $archivo = $this->request->getFile('fichero');
        $name = $archivo->getClientName(); //nombre del fichero que subimos
        $archivo->store('pruebaimages',$name); //prueba sería la carpeta que está en writable/uploads
        //<?php  echo sprintf("09%04d\n", 5);
        if ($archivo->hasMoved()) {
            echo 'El fichero se ha subido.';
        }

    }
}
