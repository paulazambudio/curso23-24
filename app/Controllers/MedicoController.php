<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use App\Models\MedicoModel;
use App\Models\EspecialidadModel;

/**
 * Description of MedicoController
 *
 * @author a023319134d
 */
class MedicoController extends BaseController {
// controlador que mostrará todos los medicos
    public function index(){
        $medicos = new MedicoModel();
	$data['titulo'] = 'Medicos'; 
        $data['medicos'] = $medicos
                ->select('medicos.id,medicos.apellido1,medicos.apellido2')
                ->select('medicos.nombre,medicos.email, especialidades.nombre as especialidades')//CUIDADO, se ralla pq son dos nombre, por eso se hace as
                ->join('especialidades', 'especialidades.id=medicos.especialidades', 'LEFT')
                //->where('especialidades.id >= 5')
                ->findAll();
        //para ver en SQL los datos que estaríamos SELECCIONANDO
        /*$sql = $medicos->builder()
                ->select('medicos.id,upper(medicos.apellido1) as apellido1,medicos.apellido2')
                ->select('medicos.nombre,medicos.email, especialidades.especialidad')
                ->join('especialidades', 'especialidades.id=medicos.especialidad', 'LEFT')
                ->where('especialidades.id >= 1')
                ->getCompiledSelect();
        print $sql;*/

        
        return view('/dentista/medico/Tablamed',$data);
    }
    
    
    public function form(){
        Helper('form');
        $data['titulo'] = 'Alta Médico';
        
        $especialidad = new EspecialidadModel();
        $especialidades = $especialidad->findAll(); 
        //cambiar estructura array
        foreach($especialidades as $especialidad){
            $listaEspecialidades[$especialidad->id]=$especialidad->nombre;
        }
        $data['especialidades'] = $listaEspecialidades;
        /*echo '<pre>';
        print_r($especialidades);
        print_r($listaEspecialidades);
        echo '</pre>';*/
        if (strtoupper($this->request->getMethod())=='GET'){ //mostramos el formulario
            
            return view('dentista/medico/Form',$data); 
        } else {
            $medico = $this->request->getPost();
            unset($medico['enviar']);
            $medicoModel = new MedicoModel(); //crear el objeto
            if ($medicoModel->insert($medico)=== false){ //He encontrado un error
                //quiero mostrar los errores
                $data['errores'] = $medicoModel->errors();
                return view('dentista/medico/Form',$data);
            } else {
                return redirect('medicos');
            }
        }

        
        
        //return view('/dentista/medico/Form',$data);
    }
    
    public function guardar(){
        
        $medico = $this->request->getPost();
        unset($medico['enviar']);
        echo '<pre>';
        print_r($medico);
        echo '</pre>';

        $medicoModel = new MedicoModel(); //crear el objeto
        $medicoModel->insert($medico);
        //return redirect('medicos');

    }
    
    public function form_edit($id){
        Helper('form');
        $data['titulo'] = 'Modificación Médico';
        
        $especialidad = new EspecialidadModel();
        $especialidades = $especialidad->findAll(); 
        //cambiar estructura array
        foreach($especialidades as $especialidad){
            $listaEspecialidades[$especialidad->id]=$especialidad->nombre;
        }
        $data['especialidades'] = $listaEspecialidades;
        
    //de aquí pabajo es distinto al form de entrada
        $medicoModel = new MedicoModel(); //para acceder a la BD
        $data['medico'] = $medicoModel->find($id);//esta id la tengo que poner en el parámetro de form_edit para decirle a quién edito
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('dentista/medico/Form_edit',$data);
        } else {
            $medico = $this->request->getPost();
            unset($medico['enviar']);
            $medico['id'] = $id; //para que funcione la regla de validación del email en edición en id
            
            $medicoModel = new MedicoModel(); //crear el objeto
            if ($medicoModel->update($id,$medico)===false){
               //quiero mostrar los errores
                $data['errores'] = $medicoModel->errors();
                return view('dentista/medico/Form_edit',$data); 
            } else {
                return redirect('medicos');
            }
        }

        
        //la ruta de la vista tb cambia
        //return view('dentista/medico/Form_edit',$data);
    }
    
    
    public function guardar_edit($id){
        
        $medico = $this->request->getPost();
        unset($medico['enviar']);
        echo '<pre>';
        echo $id;
        print_r($medico);
        echo '</pre>';
        $medicoModel = new MedicoModel(); //crear el objeto
        $medicoModel->update($id,$medico);
        //return redirect('medicos');

    }
    

    
    public function borrar($id){
        $medicoModel = new MedicoModel(); //crear el objeto
        //borrar
        $medicoModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('medicos');

    }   
    
    //función para subir foto en el alta del médico
    
}