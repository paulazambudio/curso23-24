<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\SalaModel;

/**
 * Description of SalaController
 *
 * @author a023319134d
 */
class SalaController extends BaseController {
// controlador que mostrará todos las salas
    public function index(){
        $salas = new SalaModel();
	$data['titulo'] = 'Salas'; 
        $data['salas'] = $salas->findAll();
        return view('/dentista/sala/Tablasala',$data);
    }
     public function formsala(){
        Helper('form');
        $data['titulo'] = 'Añadir Sala';
        
        if (strtoupper($this->request->getMethod())=='GET'){ //mostramos el formulario
            
            return view('dentista/sala/Formsala',$data); 
        } else {
            $sala = $this->request->getPost();
            unset($sala['enviar']);
            $salaModel = new SalaModel(); //crear el objeto
            if ($salaModel->insert($sala)=== false){ //He encontrado un error
                //quiero mostrar los errores
                $data['errores'] = $salaModel->errors();
                return view('dentista/sala/Formsala',$data);
            } else {
                return redirect('salas');
            }
        }
        
        //return view('/dentista/sala/Formsala',$data);
    }
     public function guardarsala(){
        $sala = $this->request->getPost();
        unset($sala['enviar']);
        /*echo '<pre>';
        print_r($cliente);
        echo '</pre>';*/
        $salaModel = new SalaModel(); //crear el objeto
        $salaModel->insert($sala);
        return redirect('salas');

    }
    public function formsala_edit($id){
        Helper('form');
        $data['titulo'] = 'Modificación Sala';
        
               
    //de aquí pabajo es distinto al form de entrada
        $salaModel = new SalaModel(); //para acceder a la BD
        $data['sala'] = $salaModel->find($id);//esta id la tengo que poner en el parámetro de form_edit para decirle a quién edito
        
        
        if (strtoupper($this->request->getMethod())=='GET'){
            return view('dentista/sala/Formsala_edit',$data);
        } else {
            $sala = $this->request->getPost();
            unset($sala['enviar']);
            $sala['id'] = $id;
            
            $salaModel = new SalaModel(); //crear el objeto
            if ($salaModel->update($id,$sala)===false){
               //quiero mostrar los errores
                $data['errores'] = $salaModel->errors();
                return view('dentista/sala/Formsala_edit',$data); 
            } else {
                return redirect('salas');
            }
        }
        
      
        //nos muestra el formulario para editar
        //return view('dentista/sala/Formsala_edit',$data);
        
    }
    public function guardarsala_edit($id){
        
        $sala = $this->request->getPost();
        unset($sala['enviar']);
        //echo '<pre>';
        //print_r($especialidad);
        //echo '</pre>';
        $salaModel = new SalaModel(); //crear el objeto
        $salaModel->update($id,$sala);
        return redirect()->to('salas');

    }
    public function borrarsala($id){
        $salaModel = new salaModel(); //crear el objeto
        //borrar
        $salaModel
                ->where(['id'=>$id])
                ->delete();
        return redirect()->to('salas');

    } 
}