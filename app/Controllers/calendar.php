<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/***************************************************************
 * Description of calendar
 * estamos intentando hacer 1 calendario con bootstrap y php :)
 * @author a023319134d
 **************************************************************/

class calendar extends BaseController{
    /********************************************************************
     * aquí vamos a introducir todas las funciones referentes a calendario
     * la idea es que la VISTA no tenga apenas info (parámetros) sino que eso
     * esté en el controlador (el que piensa)
     */
    public function index(){
        phpinfo();
    }

    public function calendar($num = 6) {
        $data['numero'] = $num;
        return view('tabla/tabla', $data);
    }
    
    public function mues_mes($mes=10, $anyo=2023) {
        $fecha = strtotime("$anyo-$mes-01");
        echo date('w',$fecha),'<br>';//w es el dia de la semana 0 domingo y sabado 6 pq ingles
        echo date('t',$fecha),'<br>';//t es la cantidad de dias que tiene el mes
        echo date('d-m-y',$fecha),'<br>';
        echo date("l");//Nos dice el dia en el que estamos
        
        echo '<br>';
       
        /****************************
        for($i=1;$i<=35;$i++){
            echo "[ $i ]";
            if ($i%7==0) {
                echo '<br>';
            }
        }
        *****************************/
        echo '<br>';
        $empezar = date('w',$fecha);
        if ($empezar > 0){
            $empezar;
        } else {
            $empezar = 7;
        }
        $acabar = date('t',$fecha)+$empezar;
        $i=1; //contador de cuadros
        $dia=1;//contador de dias
        while($i<=42){
            if ($i < $empezar || $i >= $acabar){
                echo '[  ]';
            } else {
                echo "[ $dia ]";
                $dia++;
            }
            if ($i%7==0) {
                echo '<br>';
            }
            $i++;
        }
        
    }
    public function muestra_mes($mes=10, $anyo=2023) {
        $fecha = strtotime("$anyo-$mes-01");
        echo date('w',$fecha),'<br>';//w es el dia de la semana 0 domingo y sabado 6 pq ingles
        echo date('t',$fecha),'<br>';//t es la cantidad de dias que tiene el mes
        echo date('d-m-y',$fecha),'<br>';
        
        echo '<br>';

        echo '<br>';
        $empezar = date('w',$fecha);
        if ($empezar > 0){
            $empezar;
        } else {
            $empezar = 7;
        }
        $acabar = date('t',$fecha)+$empezar;
        $i=1; //contador de cuadros
        $dia=1; //contador de días del 
        while($i<=42){
           if ($i < $empezar || $i >= $acabar){
              echo '[  ]';
           } else {
              echo "[ $dia ]";
              $dia++;
           }
           if ($i%7==0) {
               echo '<br>';
           }
           $i++;
        }
        
    }
    
    public function muestra_mes_2(int $mes = 10, $anyo = 2023) {
        echo '<h1>Calendario</h1>';
        $fecha = strtotime("$anyo-$mes-01");
        echo date('d-m-y', $fecha);
        $empezar = date('w', $fecha);
        if ($empezar == 0) {
            $empezar--;
        } else {
            $empezar = 6;
        }
        //primer bucle
        for ($i = 1; $i <= $empezar; $i++) {
            echo "[ ]";
        }
        //segundo bucle
        $dias_mes = date('t', $fecha);
        for ($i = 1; $i <= $dias_mes; $i++) {
            echo "[ $i ]";
            if (($i + $empezar) % 7 == 0) {
                echo '<br>';
            }
        }
        //tercer bucle
        $resto = ($dias_mes + $empezar) % 7;
        for ($i = $resto; $resto <= 7; $i++) {
            echo "[ ]";
        }
    }

}
