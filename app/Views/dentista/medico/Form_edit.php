<?= $this->extend('templates/FormsTempl') ?>

<?= $this->section('title')?> 
    <?=$titulo ?>
<?= $this->endSection() ?>

<?= $this->section('contentform')?> 
            <?= form_open('medicos/editar/'.$medico->id) ?>   
                <div class="form-group">
                    <?= form_label('Nombre:','nombre') ?>
                    <?= form_input('nombre',$medico->nombre,['id'=>'nombre','class'=>'form-control'])?>
                </div>
                <div class="form-group">
                    <?= form_label('1er Apellido:','apellido1') ?>
                    <?= form_input('apellido1',$medico->apellido1,['id'=>'apellido1','class'=>'form-control'])?>
                </div>
                <div class="form-group">
                    <?= form_label('2º Apellido:','apellido2') ?>
                    <?= form_input('apellido2',$medico->apellido2,['id'=>'apellido2','class'=>'form-control'])?>
                </div>
                <div class="form-group">
                    <?= form_label('Email:','email') ?>
                    <?= form_input('email',$medico->email,['id'=>'email','class'=>'form-control'])?>
                </div>
                <div class="form-group">
                    <?= form_label('Especialidad:','especialidad')?>
                    <?= form_dropdown('especialidades',$especialidades,$medico->especialidades,['class'=>"form-control",'id'=>'nombre'])?>
                </div>


<?= $this->endsection('contentform')?> 
