<?= $this->extend('templates/default') ?>

<?= $this->section('title')?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>

        
        <table id="mytabla" class="table table-stripped table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Foto</th>
                    <th>NameImg</th>
                    <th>Apellidos</th>
                    <th>Nombre</th>
                    <th>E-mail</th>
                    <th>Especialidades</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($medicos as $medico): ?> <!--RECORDAD : en html es el equivalente a  { --> 
                <tr>
                    <td><?= $medico-> id ?></td>
                    <td><img src="medico/fotos/09<?= str_pad($medico->id,4,'0',STR_PAD_LEFT)?>.jpg" width="30px"></td>
                    <td>09<?= str_pad($medico->id,4,'0',STR_PAD_LEFT)?>.jpg</td>
                    <td><?= $medico->apellido1 ?> <?= $medico->apellido2 ?></td>
                    <td><?= $medico->nombre ?></td>
                    <td><?= $medico->email ?></td>
                    <td><?= $medico->especialidades ?></td>
                    <td>
                        <a href="<?= site_url('medicos/editar/'.$medico->id)?>">
                            <span class="bi bi-pencil"></span></a>
                        <a href="<?= site_url('medicos/borrar/'.$medico->id)?>" onclick="return confirm('Estás seguro de que quieres borrar el médico seleccionado')">
                            <span class="bi bi-trash text-danger"></span></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
<?= $this->endSection()?>
