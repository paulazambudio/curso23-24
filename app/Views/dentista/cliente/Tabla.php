<?= $this->extend('templates/default') ?> <!-- Lo conectamos con la plantilla -->

<!-- Aquí se crea la sección, y la llamaremos en la plantilla -->
<?= $this->section('title')?> 
    <?= $titulo ?>
<?= $this->endSection() ?>


<!-- Creo la sección y será llamada en la plantilla -->
<?= $this->section('content')?> 

        
        <table id="mytabla" class="table table-stripped table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Apellidos</th>
                    <th>Nombre</th>
                    <th>E-mail</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($clientes as $cliente): ?> <!--RECORDAD : en html es el equivalente a  { --> 
                <tr>
                    <td><?= $cliente-> id ?></td>
                    <td><?= $cliente->apellido1 ?> <?= $cliente->apellido2 ?></td>
                    <td><?= $cliente->nombre ?></td>
                    <td><?= $cliente->email ?></td>
                    <td>
                        <a href="<?= site_url('clientes/editar/'.$cliente->id)?>">
                            <span class="bi bi-pencil"></span></a>
                        <a href="<?= site_url('clientes/borrar/'.$cliente->id)?>" onclick="return confirm('Estás seguro de que quieres borrar el médico seleccionado')">
                            <span class="bi bi-trash text-danger"></span></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
<?= $this->endSection()?>
