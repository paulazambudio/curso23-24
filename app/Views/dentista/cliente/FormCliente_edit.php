<?= $this->extend('templates/FormsTempl') ?>

<?= $this->section('title')?> 
    <?=$titulo ?>
<?= $this->endSection() ?>

<?= $this->section('contentform')?> 
            <?= form_open('clientes/editar/'.$cliente->id) ?>
		<div class="form-group">
                    <?= form_label('Nombre:','nombre')?>
                    <?= form_input('nombre',$cliente->nombre,['id'=>'nombre','class'=>'form-control', 'placeholder' => 'John']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('1º Apellido:','apellido1')?>
                    <?= form_input('apellido1',$cliente->apellido1,['id'=>'apellido1','class'=>'form-control', 'placeholder' => 'Doe']) ?>
        	</div>
                <div class="form-group">
                    <?= form_label('2º Apellido:','apellido2')?>
                    <?= form_input('apellido2',$cliente->apellido2,['id'=>'apellido2','class'=>'form-control', 'placeholder' => 'Deck']) ?>
		</div>
		<div class="form-group">
                    <?= form_label('E-mail:','email')?>
                    <?= form_input('email',$cliente->email,['id'=>'email','class'=>'form-control', 'placeholder' => 'johndoe@example.net']) ?>
		</div>


<?= $this->endsection('contentform')?> 