<?= $this->extend('templates/FormsTempl') ?>

<?= $this->section('title')?> 
    <?=$titulo ?>
<?= $this->endSection() ?>

<?= $this->section('contentform')?> 
            <?= form_open('clientes/form') ?>
		<div class="form-group">
                    <!--<label for="nombre"> Nombre: </label>-->
                    <?= form_label('Nombre:','nombre')?>
                    <!--<input type="text" name="nombre" class="form-control" id="nombre">-->
                    <?= form_input('nombre',set_value('nombre',''),['id'=>'nombre','class'=>'form-control', 'placeholder' => 'John']) ?>
		</div>

		<div class="form-group">
                    <?= form_label('1º Apellido:','apellido1')?>
                    <?= form_input('apellido1',set_value('apellido1',''),['id'=>'apellido1','class'=>'form-control', 'placeholder' => 'Doe']) ?>
        	</div>

                <div class="form-group">
                    <?= form_label('2º Apellido:','apellido2')?>
                    <?= form_input('apellido2',set_value('apellido2',''),['id'=>'apellido2','class'=>'form-control', 'placeholder' => 'Deck']) ?>
		</div>

		<div class="form-group">
                    <?= form_label('E-mail:','email')?>
                    <?= form_input('email',set_value('email',''),['id'=>'email','class'=>'form-control', 'placeholder' => 'johndoe@example.net']) ?>
		</div>


<?= $this->endsection('contentform')?> 