<?= $this->extend('templates/default') ?>

<?= $this->section('title')?>
    <?= $titulo ?>
<?= $this->endSection() ?>

<?= $this->section('content')?>

        
        <table id="mytabla" class="table table-stripped table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($especialidades as $especialidad): ?> <!--RECORDAD : en html es el equivalente a  { --> 
                <tr>
                    <td><?= $especialidad-> id ?></td>
                    <td><?= $especialidad->nombre ?></td>
                    <td>
                        <a href="<?= site_url('especialidades/editar/'.$especialidad->id)?>">
                            <span class="bi bi-pencil"></span></a>
                        <a href="<?= site_url('especialidades/borrar/'.$especialidad->id)?>" onclick="return confirm('Estás seguro de que quieres borrar el médico seleccionado')">
                            <span class="bi bi-trash text-danger"></span></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
<?= $this->endSection()?>
