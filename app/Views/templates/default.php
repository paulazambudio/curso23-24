<html>
    <head>
        <title>
            <?= $this->renderSection('title',true)?>
        </title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap4.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    </head>
    <body>
        <div class="container">
            <h2 class='text-danger'><?= $this->renderSection('title')?></h2>
            <?= $this->renderSection('content')?> <!-- Llamo la sección, es como hacer un copia de allí y un pega aquí -->
        </div>
        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap4.js"></script>
        <script>
            $(document).ready( function () {
                $('#mytabla').DataTable();
            } );
        </script>    
    </body>
</html>
