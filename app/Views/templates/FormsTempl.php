<html>
    <head>
        <title>
            <?= $this->renderSection('title',true)?>
        </title>
    	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">
    	<link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.css" rel="stylesheet">
    </head>
    <body>
    	<div class="container">
    	
            <h2><?= $this->renderSection('title')?></h2>
        
            <?php if (!empty($errores)): ?>
                <div class="alert alert-danger">
                    <?php foreach ($errores as $field => $error): ?>
                        <p><?= $field.' - '.$error ?></p>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
            
            <?= $this->renderSection('contentform')?>
            
		<!--<button type="submit" class="btn btn-primary">Guardar</button>-->
                <?= form_submit('enviar','Guardar',['class'=>'btn btn-primary']) ?>
            <?= form_close() ?>
        	<!--</form>-->
🙂         	 
    	</div>
    </body>
</html>