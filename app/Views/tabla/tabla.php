<!DOCTYPE html>
<html lang="es">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <h2> La tabla del <?= $numero ?> </h2>
    <table class="p-5 bg-warning text-white text-center">
        <?php for($i=0;$i<=10;$i++): ?> <!-- CUIDADO en html no es {}, es : --> 
            <tr >
		<td><?= $i ?></td>
		<td> x </td>
                <td><?= $numero ?></td>
		<td> = </td>
                <td><?= $i * $numero ?></td>
            </tr>
        <?php endfor; ?>
    </table>

</body>
</html>